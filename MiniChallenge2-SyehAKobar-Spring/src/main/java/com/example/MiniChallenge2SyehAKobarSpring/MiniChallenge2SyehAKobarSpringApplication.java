package com.example.MiniChallenge2SyehAKobarSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniChallenge2SyehAKobarSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniChallenge2SyehAKobarSpringApplication.class, args);
	}

}
