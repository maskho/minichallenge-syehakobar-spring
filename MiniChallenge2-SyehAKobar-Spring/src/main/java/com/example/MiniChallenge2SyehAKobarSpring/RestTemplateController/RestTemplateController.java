package com.example.MiniChallenge2SyehAKobarSpring.RestTemplateController;

import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class RestTemplateController {
    @GetMapping("/users")
    public void getPage() {
        RestTemplate restTemplate = new RestTemplate();
        String page = "http://reqres.in/api/users?page=2";

        ResponseEntity<String> responseEntity = restTemplate
                .getForEntity(page, String.class);
        System.out.println("resp: " + responseEntity.getStatusCode());
        System.out.println("resp: " + responseEntity.getHeaders());
        System.out.println("resp: " + responseEntity.getBody());
    }

    @GetMapping("/user")
    public void getUser() {
        RestTemplate restTemplate = new RestTemplate();
        String user = "https://reqres.in/api/users/2";

        ResponseEntity<String> responseEntity = restTemplate
                .getForEntity(user, String.class);
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getHeaders());
        System.out.println(responseEntity.getBody());
    }

    @GetMapping("/unknown")
    public void getUnknown() {
        RestTemplate restTemplate = new RestTemplate();
        String unk = "https://reqres.in/api/unknown/2";

        ResponseEntity<String> responseEntity = restTemplate
                .getForEntity(unk, String.class);
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getHeaders());
        System.out.println(responseEntity.getBody());
    }

    @GetMapping("/posts")
    public void getAllPostAPI() {
        RestTemplate restTemplate = new RestTemplate();
        String postAPI = "https://jsonplaceholder.typicode.com/posts";

        ResponseEntity<String> responseEntity = restTemplate
                .getForEntity(postAPI, String.class);
        System.out.println("response code:" + responseEntity.getStatusCode());
        System.out.println("response code:" + responseEntity.getHeaders());
        System.out.println("response code:" + responseEntity.getBody());
    }

    @PostMapping("/userpost")
    public void createPost(){
        RestTemplate restTemplate = new RestTemplate();
        String postAPI = "https://reqres.in/api/users";

        //set header
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        //set payload
        JSONObject postObject = new JSONObject();
        postObject.put("name","Dimas Adimas");
        postObject.put("job","Aktor Nollywood");

        //request post
        HttpEntity<String> request = new HttpEntity<String>(
                postObject.toString(), headers
        );
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                postAPI,request,String.class
        );
        System.out.println("Response code:: "+responseEntity.getStatusCode());
        System.out.println("Response body:: "+responseEntity.getBody());
        System.out.println("Response header:: "+responseEntity.getHeaders());
    }
}
