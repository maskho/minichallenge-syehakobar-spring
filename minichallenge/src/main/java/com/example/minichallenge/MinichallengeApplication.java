package com.example.minichallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MinichallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MinichallengeApplication.class, args);
	}

}
