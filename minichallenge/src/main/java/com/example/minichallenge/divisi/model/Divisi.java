package com.example.minichallenge.divisi.model;

import com.example.minichallenge.employee.model.Employee;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "divisi")
@Data
public class Divisi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String namaDivisi;

    @ManyToMany(mappedBy = "divisiList")
    @JsonIgnore
    private List<Employee> employeeList;
}
