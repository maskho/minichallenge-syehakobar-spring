package com.example.minichallenge.divisi.repository;

import com.example.minichallenge.divisi.model.Divisi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoDivisi extends JpaRepository<Divisi, Long> {
    public Divisi findByNamaDivisi(String nama);
}
