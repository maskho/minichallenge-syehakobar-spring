package com.example.minichallenge.employee.controller;


import com.example.minichallenge.employee.model.dto.ListDTO;
import com.example.minichallenge.employee.service.ServiceEmployee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CtrlerEmployee {
    @Autowired
    private ServiceEmployee serviceEmployee;

    @GetMapping("/employees")
    public ResponseEntity<List<ListDTO>> getAllEmployees() {
        List<ListDTO> listDTOS = serviceEmployee.getAllEmployee();
        return new ResponseEntity<>(listDTOS, HttpStatus.OK);
    }

    @PostMapping("/employee")
    public ResponseEntity<ListDTO> newEmployee(@RequestBody ListDTO listDTO) {
        ListDTO emp = serviceEmployee.addEmployee(listDTO);
        return new ResponseEntity<>(emp, HttpStatus.CREATED);
    }

    @PutMapping("/employee/{id}")
    public ResponseEntity<ListDTO> updateEmployee(@PathVariable(name = "id") Long id,
                                                  @RequestBody ListDTO listDTO) {
        ListDTO emp = serviceEmployee.updateEmployee(id, listDTO);
        return new ResponseEntity<>(emp, HttpStatus.CREATED);
    }

    @DeleteMapping("/employee/{id}")
    public ResponseEntity<String> deleteEmployee(@PathVariable(name = "id") Long id) {
        String msg = serviceEmployee.deleteEmployee(id);
        return new ResponseEntity<>(msg, HttpStatus.OK);
    }

}
