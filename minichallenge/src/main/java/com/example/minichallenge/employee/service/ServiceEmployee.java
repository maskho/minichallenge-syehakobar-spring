package com.example.minichallenge.employee.service;

import com.example.minichallenge.employee.model.dto.ListDTO;

import java.util.List;

public interface ServiceEmployee {
    ListDTO addEmployee(ListDTO listDTO);

    List<ListDTO> getAllEmployee();

    ListDTO updateEmployee(Long id_emp, ListDTO listDTO);

    String deleteEmployee(Long id_emp);
}
