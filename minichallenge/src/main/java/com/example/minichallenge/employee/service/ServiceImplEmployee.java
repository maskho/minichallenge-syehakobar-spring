package com.example.minichallenge.employee.service;

import com.example.minichallenge.divisi.model.Divisi;
import com.example.minichallenge.employee.model.Employee;
import com.example.minichallenge.employee.model.dto.ListDTO;
import com.example.minichallenge.divisi.repository.RepoDivisi;
import com.example.minichallenge.employee.repository.RepoEmployee;
import com.example.minichallenge.employee.service.ServiceEmployee;
import com.example.minichallenge.util.ModelMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ServiceImplEmployee implements ServiceEmployee {
    @Resource
    private RepoEmployee repoEmployee;
    @Resource
    private RepoDivisi repoDivisi;
    @Autowired
    private ModelMapperUtil modelMapperUtil;

    @Transactional
    @Override
    public ListDTO addEmployee(ListDTO listDTO) {
        Employee employee = new Employee();
        mapDtoToEntity(listDTO, employee);
        Employee savedEmployee = repoEmployee.save(employee);
        return mapEntityToDto(savedEmployee);
    }

    @Override
    public List<ListDTO> getAllEmployee() {
        List<ListDTO> listDTOS = new ArrayList<>();
        List<Employee> employees = repoEmployee.findAll();
        employees.stream().forEach(employee -> {
            ListDTO listDTO = mapEntityToDto(employee);
            listDTOS.add(listDTO);
        });
        return listDTOS;
    }

    @Transactional
    @Override
    public ListDTO updateEmployee(Long id, ListDTO listDTO) {
        Employee employee = repoEmployee.getOne(id);
        employee.getDivisiList().clear();
        mapDtoToEntity(listDTO, employee);
        Employee employee1 = repoEmployee.save(employee);
        return mapEntityToDto(employee1);
    }

    @Override
    public String deleteEmployee(Long idEmployee) {
        Employee employee = repoEmployee.getOne(idEmployee);
        employee.removeDivisiList();
        repoEmployee.deleteById(idEmployee);
        return "Employee dengan id: " + idEmployee + " dihapus";
    }

    private void mapDtoToEntity(ListDTO listDTO, Employee employee) {
        employee.setNamaEmployee(listDTO.getNamaEmployee());
        if (null == employee.getDivisiList()) {
            employee.setDivisiList(new ArrayList<>());
        }
        listDTO.getDivisiList().stream().forEach(namaDiv -> {
            Divisi divisi = repoDivisi.findByNamaDivisi(namaDiv);
            if (null == divisi) {
                divisi = new Divisi();
                divisi.setEmployeeList(new ArrayList<>());
            }
            divisi.setNamaDivisi(namaDiv);
            employee.addDivisi(divisi);
        });
    }

    private ListDTO mapEntityToDto(Employee employee) {
        ListDTO responseDTO = new ListDTO();
        responseDTO.setNamaEmployee(employee.getNamaEmployee());
        responseDTO.setId(employee.getId());
        responseDTO.setDivisiList(employee.getDivisiList().stream().map(Divisi::getNamaDivisi).collect(Collectors.toList()));
        return responseDTO;
    }
}
