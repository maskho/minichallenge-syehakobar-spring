package com.example.minichallenge.employee.repository;

import com.example.minichallenge.employee.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepoEmployee extends JpaRepository<Employee, Long> {
}
