package com.example.minichallenge.employee.model;

import com.example.minichallenge.divisi.model.Divisi;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employee")
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nama")
    private String namaEmployee;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "emp_div",
            joinColumns = @JoinColumn(name = "id_divisi",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_employee",
                    referencedColumnName = "id")
    )
    private List<Divisi> divisiList;

    public void addDivisi(Divisi divisi) {
        this.divisiList.add(divisi);
        divisi.getEmployeeList().add(this);
    }

    public void removeDivisi(Divisi divisi) {
        this.getDivisiList().remove(divisi);
        divisi.getEmployeeList().remove(this);
    }

    public void removeDivisiList() {
        for (Divisi divisi : new ArrayList<>(divisiList)) {
            removeDivisi(divisi);
        }
    }
}
