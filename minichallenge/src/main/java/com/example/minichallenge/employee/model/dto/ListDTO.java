package com.example.minichallenge.employee.model.dto;

import lombok.Data;

import java.util.List;

@Data
public class ListDTO {
    private Long id;
    private String namaEmployee;
    private List<String> divisiList;
}
